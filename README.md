# Clickrobot

A bookmarklet that automates the repeated clicking of an element on the web page.

![clickrobot bookmarklet](https://i.imgur.com/fREFaAP.png)

http://desbest.com/clickrobot

## More Information

Are you sick of wasting your time and crashing the computer, repeatedly clicking the More button on Quora, wishing for a way to view all your answers at once?

Using Clickrobot, you would be able to have the computer automate clicking the more button for you, saving you lots of time, and the computer won't crash too.

Just drag the bookmarklet below to your bookmarks toolbar, or bookmark it somewhere.

[Install or test out the bookmarklet](https://desbest.com/clickrobot/)